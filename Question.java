import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.jws.Oneway;
import javax.persistence.*;

import javax.xml.bind.annotation.*;

@XmlRootElement
@Entity public class Question implements Serializable {
	@Id @GeneratedValue private int id;
	@XmlElement
    private String text;
    @XmlElement
    @OneToMany private List<Choice> choices = new ArrayList<Choice>();
    @XmlElement
    @OneToOne private Choice answer;
	
	public Question() {}
	
    public void read(Scanner in) {
        text = in.nextLine();
        while (in.hasNextLine()) {
            String line = in.nextLine();
            boolean isAnswer = false;
            if (line.startsWith("*")) { isAnswer = true; line = line.substring(1); }
            Choice choice = new Choice(line);
            choices.add(choice);
            if (isAnswer) answer = choice;            
        }
    }
	
    public List<Choice> getChoices() { return Collections.unmodifiableList(choices); }
    public String getText() { return text; }
    public Choice getAnswer() { return answer; }
    public int getId() { return id; }
    public String toString() {
        String result = text + "\n";
        for (Choice c : choices) {
            if (c.getText().equals(answer.getText())) result += "*";
            result += c.getText() + "\n";
        }
        return result;
    }
}