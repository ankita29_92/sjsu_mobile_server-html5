import javax.persistence.*;
import javax.xml.bind.annotation.*;

@XmlRootElement
@Entity public class Choice {
	@Id @GeneratedValue private int id;
	@XmlElement
    private String text;
	
	public Choice() {}
	
    public Choice(String text) { this.text = text; }
    public String getText() { return text; }
    public String toString() {
    	return text;
    }
}