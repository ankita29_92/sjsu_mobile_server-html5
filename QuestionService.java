import java.util.Scanner;
import javax.ejb.*;
import javax.persistence.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/question")
@Stateless
public class QuestionService {
	@PersistenceContext(unitName="team3")
	private EntityManager em;

	@GET
	@Path("/init")
	@Produces("text/plain")
	public Response init() {
		String[] inputs = {
				"Which is the most popular mobile OS?\n*Android\niOS\nBlackberry\nWindows 8\n",
				"Which smarthone does not run on Android?\n*Nokia N8\nSamsung Galaxy\nMotorola Droid\nSprint Evo 4G\n",
				"Which year did Google purchase Android?\n2004\n*2005\n2006\n2007\n",
				"What was the first commercially available Android phone?\nSamsung Galaxy\nMotorola Droid\nGoogle Nexus One\n*HTC Dream\n"
		};

		String ids = "";
		for (int i = 0; i < inputs.length; i++) {
			Question q = new Question();
			q.read(new Scanner(inputs[i]));
			for (Choice c : q.getChoices()) em.persist(c);
			em.persist(q);
			if (i > 0) ids += ", ";
			ids += q.getId();
		}

		return Response.status(200).entity("" + ids).build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response getQuestion(@PathParam("id") int id) {
		Query q = em.createQuery("SELECT q FROM Question q WHERE q.id = :id").setParameter("id", id);
		Question p = (Question) q.getSingleResult();
		return Response.status(200).entity(p).build();
	}
}
