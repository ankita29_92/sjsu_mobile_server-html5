<!DOCTYPE html>
<html> 
 <head> 
    <title>Quiz App</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1"/> 
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.css" />            
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.js"></script>
        <script type="text/javascript">
            var question;
            var ids;
            var score = 0;
            var count = 0;

            function fetchIDs() {
            	$.ajax({
				    async: false,
				    type: 'GET',
				    url: 'quizapp/question/init',
				    success: function(data) {
				    	ids = data.split(", ");
				    }
				});
            }

            function fetchQuestion() {
            	$.getJSON('quizapp/question/'+ids[count], function(data) {
					question = data;
					$('a').click(function(event) { alert(event.target.firstChild.textContent) });
					$('#questionText').html('<h2>'+question.text+'</h2>');
					var choices = $('#choices');
					for (var i = 0; i < question.choices.length; i++) {
						var item = $('<li/>');
						var link = $('<a/>').html(question.choices[i].text);
						item.append(link);
						choices.append(item);
						link.click(function(e) {
							if ($(e.target).text() == question.answer.text)
								$.mobile.changePage('#correct');
							else
								$.mobile.changePage('#incorrect');
						})
					}
					choices.listview('refresh');
					count++;
				});
            }

            function computeScore() {
            	var percentage = parseInt(100 * score / ids.length, 10);
            	$('#scorePercentage').html("<h2>"+percentage+"%</h2>");
            	$('#scoreText').html("You scored "+score+" out of "+ids.length+".");
            }

			$(document).ready(function() {
				//get question ids from the init page
				fetchIDs();

				//fetch the first question
				fetchQuestion();

				$('#retry').click( function(e) {
					count = 0;
					score = 0;
					fetchIDs();
					fetchQuestion();
					$('#choices').empty();
					$.mobile.changePage('#question');
				});

				$('#nextAfterTrue').click( function(e) {
					score += 1;
					if (count >= ids.length) {
						computeScore();
						$.mobile.changePage('#score');
					} else {
						$('#choices').empty();
						fetchQuestion();
						$.mobile.changePage('#question');
					}
				});
				$('#nextAfterFalse').click( function(e) {
					if (count >= ids.length) {
						computeScore();
						$.mobile.changePage('#score');
					} else {
						$('#choices').empty();
						fetchQuestion();
						$.mobile.changePage('#question');
					}
				});
            });
       </script>
</head> 
<body>

<div data-role="page" id="question">
    <div id="questionText" data-role="header">
    </div>

    <div data-role="content">
        <ul id="choices" data-role="listview" data-inset="true" data-filter="false">
        </ul>        
    </div> 
</div>

<div data-role="page" id="correct">             
	<div data-role="header">
		<h2>
			Congratulations!
		</h2>
	</div>

	<input id="nextAfterTrue" type="button" value="Next" />
</div>

<div data-role="page" id="incorrect">
	<div data-role="header">
		<h2>
			Wrong Answer!
		</h2>
	</div>

	<input id="nextAfterFalse" type="button" value="Next" />
</div> 

<div data-role="page" id="score">
    <div id="scorePercentage" data-role="header">
    </div>

    <div data-role="content">
    	<h3 id="scoreText">
    	</h3>

    	<input id="retry" type="button" value="Retry?" />
    </div> 
</div>

</body>
</html>